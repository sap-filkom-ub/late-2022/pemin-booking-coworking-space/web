<?php

use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\HomeController;
use App\Http\Controllers\WelcomeController;
use App\Http\Controllers\NotificationController;

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\ProfileController;

use App\Http\Controllers\Admin\FloorController;
use App\Http\Controllers\Admin\SectorController;
use App\Http\Controllers\Admin\DeskController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\SubscriptionController;
use App\Http\Controllers\Admin\OrderController;

use App\Http\Controllers\Admin\BookingController;
use App\Http\Controllers\Admin\AssessmentController;

use App\Http\Controllers\User\PageController;
use App\Http\Controllers\User\ProfileuController;
use App\Http\Controllers\User\BuyController;
use App\Http\Controllers\User\DashboarduController;
use App\Http\Controllers\User\HistoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PageController::class, 'welcome']);
// Route::get('/dashboard/{username}',[PageController::class, 'dashboard']);

Route::get('/admin', [WelcomeController::class, 'index'])->name('welcome');
Route::get('/synapsis', [WelcomeController::class, 'synapsis']);
Route::get('/activitymonth', [WelcomeController::class, 'activityMonth']);
Route::get('/logmonth', [WelcomeController::class, 'logMonth']);
// Route::get('/notification', [UserController::class, 'notification']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

$router->group(['prefix' => 'user', 'middleware' => 'is_user'], function () use ($router) {
    Route::get('profile', [ProfileuController::class, 'index'])->name('user.profile.index');
    Route::post('profile/{user}', [ProfileuController::class, 'update'])->name('user.profile.update');

    Route::get('dashboard/', [DashboarduController::class, 'index'])->name('user.dashboard');

    Route::get('history/', [HistoryController::class, 'index'])->name('user.history');
    Route::get('history/{id}', [HistoryController::class, 'show'])->name('user.history.show');

    Route::get('buy/', [BuyController::class, 'index'])->name('user.buy');
    Route::post('buy/store', [BuyController::class, 'store'])->name('user.buy.store');
});

$router->group(['prefix' => 'admin', 'middleware' => 'is_admin'], function () use ($router) {
    Route::get('dashboard', [DashboardController::class, 'index'])->name('admin.dashboard');

    Route::get('profile', [ProfileController::class, 'index'])->name('profile.index');
    Route::post('profile/{user}', [ProfileController::class, 'update'])->name('profile.update');

    Route::resource('user', UserController::class);
    Route::resource('subscription', SubscriptionController::class);
    Route::resource('order', OrderController::class);

    Route::get('floor/list', [FloorController::class, 'list'])->name('floor.list');
    Route::resource('floor', FloorController::class);

    Route::get('floor/{floor}/sector/list', [SectorController::class, 'list'])->name('floor.sector.list');
    Route::resource('floor.sector', SectorController::class);

    Route::get('floor/{floor}/sector/{sector}/desk/list', [DeskController::class, 'list'])->name('floor.sector.desk.list');
    Route::resource('floor.sector.desk', DeskController::class);

    Route::get('booking', [BookingController::class, 'index'])->name('booking.index');
    Route::get('booking/{booking}', [BookingController::class, 'show'])->name('booking.show');
    Route::get('booking/{booking}/checkout', [BookingController::class, 'checkout'])->name('booking.checkout');

    Route::get('assessment', [AssessmentController::class, 'index'])->name('assessment.index');
});
