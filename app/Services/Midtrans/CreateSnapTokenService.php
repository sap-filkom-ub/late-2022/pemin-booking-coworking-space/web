<?php

namespace App\Services\Midtrans;

use Midtrans\Snap;

class CreateSnapTokenService extends Midtrans
{
    protected $order;

    public function __construct($order)
    {
        parent::__construct();

        $this->order = $order;
    }

    public function getSnapToken()
    {
        $params = [
            'transaction_details' => [
                'order_id' => $this->order->order_id,
                'gross_amount' => $this->order->total_price,
            ],
            'item_details' => [
                [
                    'id'        => $this->order->subscription->id,
                    'price'     => $this->order->subscription->price,
                    'quantity'  => 1,
                    'name'      => $this->order->subscription->name,
                ],
            ],
            'customer_details' => [
                'first_name'    => $this->order->user->name,
                'email'         => $this->order->user->email,
                'phone'         => $this->order->user->phone,
            ]
        ];

        $snapToken = Snap::getSnapToken($params);

        return $snapToken;
    }
}
