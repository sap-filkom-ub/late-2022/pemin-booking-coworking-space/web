<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Subscription;
use Illuminate\Http\Request;

use App\Services\Midtrans\CreateSnapTokenService;
use App\Services\Midtrans\CallbackService;
use Carbon\Carbon;

use Midtrans\Transaction;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::orderBy('created_at', 'desc')->get();

        foreach ($orders as $order) {
            if (
                $order->payment_status == 'unpaid'
                || $order->payment_status == 'pending'
                || $order->payment_status == 'capture'
                || $order->payment_status == 'authorize'
                // || $order->payment_status == 'settlement' // small percentage to change
            ) {
                // will get callback
                $midtrans = new CallbackService($order);
                $order->status = $midtrans->getStatus();
                $this->update($order);
            }
        }

        return view('admin.order.index')->with(compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subscriptions = Subscription::get();
        foreach ($subscriptions as $subscription) {
            $now = Carbon::now();
            if ($subscription->unit == 'day') {
                $now->addDays($subscription->duration);
            } elseif ($subscription->unit == 'week') {
                $now->addWeeks($subscription->duration);
            } elseif ($subscription->unit == 'month') {
                $now->addMonth($subscription->duration);
            } else {
                $now->addYears($subscription->duration);
            }
            $subscription->estimate = $now->format('Y-m-d H:i');
        }
        return view('admin.order.create')->with(compact('subscriptions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // create new order
        $order = Order::create([
            'user_id'           => $request->user_id,
            'subscription_id'   => $request->subscription_id,
            'order_id'          => 'OR' . Carbon::now()->format('YmdHis'),
            'total_price'       => $request->price,
            'payment_status'    => 'unpaid',
        ]);

        // will generate new snapToken
        $midtrans = new CreateSnapTokenService($order);
        $snapToken = $midtrans->getSnapToken();
        $order->snap_token = $snapToken;
        $order->save();

        return redirect()->route('order.index')
            ->with('success', 'Order created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $snapToken = $order->snap_token;
        if (empty($snapToken)) {
            // will generate new snapToken
            $midtrans = new CreateSnapTokenService($order);
            $snapToken = $midtrans->getSnapToken();

            $order->snap_token = $snapToken;
            $order->save();
        }

        if (
            $order->payment_status == 'unpaid'
            || $order->payment_status == 'pending'
            || $order->payment_status == 'capture'
            || $order->payment_status == 'authorize'
            // || $order->payment_status == 'settlement' // small percentage to change
        ) {
            // will get callback
            $midtrans = new CallbackService($order);
            $order->status = $midtrans->getStatus();
            $this->update($order);
        }

        return view('admin.order.show', compact('order', 'snapToken'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update($order_in)
    {
        $order = Order::where('order_id', $order_in->order_id)->first();
        $unit = $order->subscription->unit;
        $duration = $order->subscription->duration;
        $status = $order->payment_status;

        try {
            $transaction_status = $order_in->status->transaction_status;
            if ($transaction_status != $status) {
                if ($transaction_status == 'settlement') {
                    $settlement_time = $order_in->status->settlement_time;
                    $payment_date = Carbon::parse($settlement_time);
                    $end_date = Carbon::parse($settlement_time);

                    if ($unit == 'day') {
                        $end_date->addDays($duration);
                    } elseif ($unit == 'week') {
                        $end_date->addWeeks($duration);
                    } elseif ($unit == 'month') {
                        $end_date->addMonth($duration);
                    } else {
                        $end_date->addYears($duration);
                    }
                    $order->payment_date = $payment_date;
                    $order->end_date = $end_date;
                }

                // save new data
                $order->payment_status = $transaction_status;
                $order->save();
            }
        } catch (\Throwable $th) {
            //throw $th;
            // $transaction_status = $order->payment_status;
            // $payment_date = NULL;
            // $end_date = NULL;
        }
        return true;
    }
}
