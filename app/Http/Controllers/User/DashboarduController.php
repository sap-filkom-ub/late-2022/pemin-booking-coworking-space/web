<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use Carbon\Carbon;

use Illuminate\Support\Facades\Auth;

use App\Services\Midtrans\CreateSnapTokenService;
use App\Services\Midtrans\CallbackService;

class DashboarduController extends Controller
{
    public function index()
    {
        $now = Carbon::now();

        $order = Order::whereDate('end_date', '>', $now->toDateString())->where('user_id', Auth::user()->id)->first();
        $snapToken = "";
        try {
            //code...
            $snapToken = $order->snap_token;
            if (empty($snapToken)) {
                // will generate new snapToken
                $midtrans = new CreateSnapTokenService($order);
                $snapToken = $midtrans->getSnapToken();

                $order->snap_token = $snapToken;
                $order->save();
            }

            if (
                $order->payment_status == 'unpaid'
                || $order->payment_status == 'pending'
                || $order->payment_status == 'capture'
                || $order->payment_status == 'authorize'
                // || $order->payment_status == 'settlement' // small percentage to change
            ) {
                // will get callback
                $midtrans = new CallbackService($order);
                $order->status = $midtrans->getStatus();
                $this->update($order);
            }
        } catch (\Throwable $th) {
            //throw $th;
        }

        return view('user.dashboard', compact('order', 'snapToken'));
    }
}
