<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Subscription;

class PageController extends Controller
{
    public function welcome()
    {
        $pricings = Subscription::get()->all();
        return view('user.welcome')->with(compact('pricings'));
    }

    public function dashboard()
    {
        return view('user.dashboard');
    }
}
