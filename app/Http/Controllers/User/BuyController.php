<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Subscription;
use Illuminate\Http\Request;

use App\Services\Midtrans\CreateSnapTokenService;
use App\Services\Midtrans\CallbackService;
use Carbon\Carbon;

use Midtrans\Transaction;

class BuyController extends Controller
{
    public function index()
    {
        $subscriptions = Subscription::get();
        foreach ($subscriptions as $subscription) {
            $now = Carbon::now();
            if ($subscription->unit == 'day') {
                $now->addDays($subscription->duration);
            } elseif ($subscription->unit == 'week') {
                $now->addWeeks($subscription->duration);
            } elseif ($subscription->unit == 'month') {
                $now->addMonth($subscription->duration);
            } else {
                $now->addYears($subscription->duration);
            }
            $subscription->estimate = $now->format('Y-m-d H:i');
        }
        return view('user.buy')->with(compact('subscriptions'));
    }

    public function store(Request $request)
    {
        // create new order
        $order = Order::create([
            'user_id'           => $request->user_id,
            'subscription_id'   => $request->subscription_id,
            'order_id'          => 'OR' . Carbon::now()->format('YmdHis'),
            'total_price'       => $request->price,
            'payment_status'    => 'unpaid',
        ]);

        // will generate new snapToken
        $midtrans = new CreateSnapTokenService($order);
        $snapToken = $midtrans->getSnapToken();
        $order->snap_token = $snapToken;
        $order->save();

        return redirect()->route('user.history')
            ->with('success', 'Order created successfully.');
    }
}
