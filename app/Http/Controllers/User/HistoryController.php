<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Subscription;
use Illuminate\Http\Request;

use App\Services\Midtrans\CreateSnapTokenService;
use App\Services\Midtrans\CallbackService;
use Carbon\Carbon;

use Midtrans\Transaction;

use Illuminate\Support\Facades\Auth;

class HistoryController extends Controller
{
    public function index()
    {
        $orders = Order::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();

        foreach ($orders as $order) {
            if (
                $order->payment_status == 'unpaid'
                || $order->payment_status == 'pending'
                || $order->payment_status == 'capture'
                || $order->payment_status == 'authorize'
                // || $order->payment_status == 'settlement' // small percentage to change
            ) {
                // will get callback
                $midtrans = new CallbackService($order);
                $order->status = $midtrans->getStatus();
                $this->update($order);
            }
        }

        return view('user.history.index')->with(compact('orders'));
    }

    public function update($order_in)
    {
        $order = Order::where('order_id', $order_in->order_id)->first();
        $unit = $order->subscription->unit;
        $duration = $order->subscription->duration;
        $status = $order->payment_status;

        try {
            $transaction_status = $order_in->status->transaction_status;
            if ($transaction_status != $status) {
                if ($transaction_status == 'settlement') {
                    $settlement_time = $order_in->status->settlement_time;
                    $payment_date = Carbon::parse($settlement_time);
                    $end_date = Carbon::parse($settlement_time);

                    if ($unit == 'day') {
                        $end_date->addDays($duration);
                    } elseif ($unit == 'week') {
                        $end_date->addWeeks($duration);
                    } elseif ($unit == 'month') {
                        $end_date->addMonth($duration);
                    } else {
                        $end_date->addYears($duration);
                    }
                    $order->payment_date = $payment_date;
                    $order->end_date = $end_date;
                }

                // save new data
                $order->payment_status = $transaction_status;
                $order->save();
            }
        } catch (\Throwable $th) {
            //throw $th;
            // $transaction_status = $order->payment_status;
            // $payment_date = NULL;
            // $end_date = NULL;
        }
        return true;
    }

    public function show($order)
    {
        $order = Order::where('id', $order)->first();
        $snapToken = $order->snap_token;
        if (empty($snapToken)) {
            // will generate new snapToken
            $midtrans = new CreateSnapTokenService($order);
            $snapToken = $midtrans->getSnapToken();

            $order->snap_token = $snapToken;
            $order->save();
        }

        if (
            $order->payment_status == 'unpaid'
            || $order->payment_status == 'pending'
            || $order->payment_status == 'capture'
            || $order->payment_status == 'authorize'
            // || $order->payment_status == 'settlement' // small percentage to change
        ) {
            // will get callback
            $midtrans = new CallbackService($order);
            $order->status = $midtrans->getStatus();
            $this->update($order);
        }

        return view('user.history.show', compact('order', 'snapToken'));
    }
}
