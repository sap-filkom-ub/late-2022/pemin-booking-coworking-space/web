@extends('layouts.app')
@section('title', 'Register')
@section('content')
    <div class="container py-4 mt-5">
        <!-- Outer Row -->
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-8">
                <div class="card o-hidden shadow-lg my-5" style="border-radius: 1.35rem">
                    <div class="card-body p-0">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf
                            <!-- Nested Row within Card Body -->
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4"><strong>{{ __('Register') }} a new
                                            account</strong>
                                    </h1>
                                </div>
                                <div class="form-group">
                                    <input id="name" type="text"
                                        class="form-control @error('name') is-invalid @enderror" name="name"
                                        value="{{ old('name') }}" required autocomplete="name" autofocus
                                        placeholder="Enter Your Name..." style="border-radius:10rem; padding:1.5rem 1rem">
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <input id="username" type="text"
                                        class="form-control @error('username') is-invalid @enderror" name="username"
                                        value="{{ old('username') }}" required autocomplete="username" autofocus
                                        placeholder="Enter Username..." style="border-radius:10rem; padding:1.5rem 1rem">
                                    @error('username')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <input id="email" type="email"
                                        class="form-control @error('email') is-invalid @enderror" name="email"
                                        value="{{ old('email') }}" required autocomplete="email" autofocus
                                        placeholder="Enter Email..." style="border-radius:10rem; padding:1.5rem 1rem">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <input id="password" type="password"
                                        class="form-control @error('password') is-invalid @enderror" name="password"
                                        required autocomplete="new-password" placeholder="Enter New Password"
                                        style="border-radius:10rem; padding:1.5rem 1rem">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group">
                                    <input id="password_confirmation" type="password"
                                        class="form-control @error('password_confirmation') is-invalid @enderror"
                                        name="password_confirmation" required autocomplete="new-password"
                                        placeholder="Enter Confirmation Password..."
                                        style="border-radius:10rem; padding:1.5rem 1rem">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col">
                                        <button type="submit" class="btn btn-primary btn-user btn-block btn-login"
                                            style="border-radius: 10rem; padding: .75rem 1rem;">
                                            {{ __('Register') }}
                                        </button>
                                        <hr>
                                        <div class="text-center">
                                            <a class="small" href="{{ route('login') }}">Login to an
                                                Account!</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
