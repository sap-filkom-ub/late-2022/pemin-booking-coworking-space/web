@extends('user.layouts.user')
@section('title', 'Order Detail')
@section('content')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('order.index') }}">Order List</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $order->order_id }}</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col"></div>
        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h5 class="m-0 font-weight-bold text-primary">Order Detail</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <table class="table mb-4">
                                <tr>
                                    <th style="width: 20%">Order Number</td>
                                    <td style="width: 0%">:</td>
                                    <td>{{ $order->order_id }}</td>
                                </tr>
                                <tr>
                                    <th>Created at</th>
                                    <td>:</td>
                                    <td>{{ $order->created_at }}</td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>:</td>
                                    <td>
                                        @if ($order->payment_status == 'unpaid')
                                            Unpaid
                                        @else
                                            Paid at {{ $order->payment_date }}
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <h5 class="font-weight-bold">User Information</h5>
                            <table class="table mb-4">
                                <tr>
                                    <th style="width: 20%">Name</td>
                                    <td style="width: 0%">:</td>
                                    <td>{{ $order->user->name }}</td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>:</td>
                                    <td>{{ $order->user->email }}</td>
                                </tr>
                            </table>
                        </div>

                        <div class="col">
                            <h5 class="font-weight-bold">Service Information</h5>
                            <table class="table mb-4">
                                <tr>
                                    <th style="width: 20%">Name</td>
                                    <td style="width: 0%">:</td>
                                    <td>{{ $order->subscription->name }}</td>
                                </tr>
                                <tr>
                                    <th>Duration</th>
                                    <td>:</td>
                                    <td>{{ $order->subscription->duration }} {{ $order->subscription->unit }}</td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col">
                            <h5 class="font-weight-bold">Service Detail</h5>
                            @if ($order->payment_status != 'settlement')
                                <div class="alert alert-danger">Can't be used yet</div>
                            @else
                                <table class="table mb-4">
                                    <tr>
                                        <th style="width: 20%">Started at</td>
                                        <td style="width: 0%">:</td>
                                        <td>
                                            @if ($order->payment_status != 'settlement')
                                                -
                                            @else
                                                {{ $order->payment_date }}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Ended at</th>
                                        <td>:</td>
                                        <td>
                                            @if ($order->payment_status != 'settlement')
                                                -
                                            @else
                                                {{ $order->end_date }}
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            @endif
                        </div>
                    </div>

                    @if ($order->payment_status == 'unpaid')
                        <button class="btn btn-primary pull-right mb-2" id="pay-button">Pay Now</button>
                    @endif
                </div>
            </div>
        </div>
        <div class="col"></div>
    </div>

@endsection

@section('script')
    <script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="{{ config('midtrans.client_key') }}">
    </script>
    <script>
        const payButton = document.querySelector('#pay-button');
        payButton.addEventListener('click', function(e) {
            e.preventDefault();

            snap.pay('{{ $snapToken }}', {
                onSuccess: function(result) {
                    window.location.reload();
                },
                onPending: function(result) {
                    window.location.reload();
                },
                onError: function(result) {
                    window.location.reload();
                },
                // onClose: function() {
                //     /* You may add your own implementation here */
                //     alert('you closed the popup without finishing the payment');
                // }
            });
        });
    </script>
@endsection
