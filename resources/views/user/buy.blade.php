@extends('user.layouts.user')
@section('title', 'Produk')

@section('style')
    {{-- Datatables --}}
    <link href="{{ asset('datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('datatables/responsive/css/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet" type="text/css">

    <style>
        #table {
            table-layout: fixed;
            width: 100% !important;
        }

        .card-text {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .btn-secondary:hover,
        .btn-secondary:focus,
        .btn-secondary:active,
        .btn-secondary.active,
        .open>.dropdown-toggle.btn-secondary {
            background-color: #366bbd;
        }
    </style>
@endsection

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('order.index') }}">Order List</a></li>
            <li class="breadcrumb-item active" aria-current="page">Choose Subscription</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col"></div>
        <div class="col-lg-8">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h5 class="m-0 font-weight-bold text-primary">Subscription List</h5>
                </div>
                <div class="card-body">

                    <div class="table-responsive">
                        <table class="table table-bordered display nowrap" id="table" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th width="4%">#</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Duration</th>
                                    <th>Price</th>
                                    <th width="9%">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($subscriptions as $subscription)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $subscription->name }}</td>
                                        <td>{{ $subscription->description }}</td>
                                        <td>{{ $subscription->duration }} {{ $subscription->unit }}</td>
                                        <td>Rp. {{ number_format($subscription->price, 0) }},-</td>
                                        <td class="text-center">
                                            <a href="#" class="btn btn-primary btn-circle mb-1" data-toggle="modal"
                                                data-target="#bookModal" data-id="{{ $subscription->id }}"
                                                data-name="{{ $subscription->name }}"
                                                data-duration="{{ $subscription->duration }} {{ $subscription->unit }}"
                                                data-price="{{ $subscription->price }}"
                                                data-estimate="{{ $subscription->estimate }}">
                                                <i class="fas fa-cart-arrow-down"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                @endforelse
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <div class="col"></div>
    </div>

    <!-- Modal -->
    <div id="bookModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Booking Detail</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordereless display nowrap" id="tableDetail" width="100%" cellspacing="0">
                        <tr>
                            <td>Name</td>
                            <td>:</td>
                            <td id="name"></td>
                        </tr>
                        <tr>
                            <td>Duration</td>
                            <td>:</td>
                            <td id="duration"></td>
                        </tr>
                        <tr>
                            <td>Price</td>
                            <td>:</td>
                            <td id="price"></td>
                        </tr>
                        <tr>
                            <td>Estimate</td>
                            <td>:</td>
                            <td>
                                <strong id="estimate"></strong><br>
                                <small>*depends on when you finish the payment</small>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">No</button>
                    <form action="#" method="POST" id="orderForm">
                        @csrf
                        <input type="hidden" value="{{ Auth::user()->id }}" name="user_id">
                        <input type="hidden" value="" id="subscription_id" name="subscription_id">
                        <input type="hidden" value="" id="hide_price" name="price">
                        <button type="submit" class="btn btn-primary" id="order-btn"><strong>Yes, order</strong></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    {{-- Datatable --}}
    <script src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('datatables/responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('datatables/responsive/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('datatables/sorting/natural.js') }}"></script>
    <script src="{{ asset('js/toastr.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            table = $('#table').DataTable({
                columnDefs: [
                    // {
                    //     type: 'natural',
                    //     targets: 1
                    // },
                    {
                        orderable: false,
                        targets: 5
                    },
                ],
                // order: [
                //     [1, 'desc']
                // ],
            });
        })
    </script>

    {{-- Midtrans --}}
    <script type="text/javascript">
        $('#bookModal').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var id = button.data('id'); // Extract info from data-* attributes

            $("#subscription_id").val(button.data('id'));
            $("#name").html(button.data('name'));
            $("#duration").html(button.data('duration'));
            $("#price").html(button.data('price'));
            $("#hide_price").val(button.data('price'));
            $("#estimate").html(button.data('estimate'));

            // var url = button.data('url') + '#toolbar=1';
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this);
            var form = document.getElementById("orderForm");
            let url = "{{ route('user.buy.store') }}";
            // url = url.replace(':id', id);
            form.action = url;
        })
    </script>
@endsection
