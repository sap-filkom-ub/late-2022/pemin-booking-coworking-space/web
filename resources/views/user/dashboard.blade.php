@extends('user.layouts.user')
@section('title', 'Dashboard')
@section('content')

    <div class="row">
        <div class="col"></div>
        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h5 class="m-0 font-weight-bold text-primary">Latest Order</h5>
                </div>
                <div class="card-body">

                    @if (!$order)
                        <div class="alert alert-danger">Can't be used yet, you have to order</div>
                    @else
                        <div class="row">
                            <div class="col">
                                <table class="table mb-4">
                                    <tr>
                                        <th style="width: 20%">Order Number</td>
                                        <td style="width: 0%">:</td>
                                        <td>{{ $order->order_id }}</td>
                                    </tr>
                                    <tr>
                                        <th>Created at</th>
                                        <td>:</td>
                                        <td>{{ $order->created_at }}</td>
                                    </tr>
                                    <tr>
                                        <th>Status</th>
                                        <td>:</td>
                                        <td>
                                            @if ($order->payment_status == 'unpaid')
                                                Unpaid
                                            @else
                                                Paid at {{ $order->payment_date }}
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <h5 class="font-weight-bold">User Information</h5>
                                <table class="table mb-4">
                                    <tr>
                                        <th style="width: 20%">Name</td>
                                        <td style="width: 0%">:</td>
                                        <td>{{ $order->user->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td>:</td>
                                        <td>{{ $order->user->email }}</td>
                                    </tr>
                                </table>
                            </div>

                            <div class="col">
                                <h5 class="font-weight-bold">Service Information</h5>
                                <table class="table mb-4">
                                    <tr>
                                        <th style="width: 20%">Name</td>
                                        <td style="width: 0%">:</td>
                                        <td>{{ $order->subscription->name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Duration</th>
                                        <td>:</td>
                                        <td>{{ $order->subscription->duration }} {{ $order->subscription->unit }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <h5 class="font-weight-bold">Service Detail</h5>
                                @if ($order->payment_status != 'settlement')
                                    <div class="alert alert-danger">Can't be used yet</div>
                                @else
                                    <table class="table mb-4">
                                        <tr>
                                            <th style="width: 20%">Started at</td>
                                            <td style="width: 0%">:</td>
                                            <td>
                                                @if ($order->payment_status != 'settlement')
                                                    -
                                                @else
                                                    {{ $order->payment_date }}
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Ended at</th>
                                            <td>:</td>
                                            <td>
                                                @if ($order->payment_status != 'settlement')
                                                    -
                                                @else
                                                    {{ $order->end_date }}
                                                @endif
                                            </td>
                                        </tr>
                                    </table>
                                @endif
                            </div>
                        </div>

                    @endif

                </div>
            </div>
        </div>
        <div class="col"></div>
    </div>

@endsection
