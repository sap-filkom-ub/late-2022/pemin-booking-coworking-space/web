@extends('admin.layouts.admin')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{ route('subscription.index') }}">Subscription List</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $subscription->name }}</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col"></div>
        <div class="col-lg-6">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h5 class="m-0 font-weight-bold text-primary">Edit Subscription</h5>
                </div>
                <div class="card-body">
                    @if (count($errors) > 0)
                        <p class="card-description">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        </p>
                    @endif
                    <form action="{{ route('subscription.update', $subscription->id) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name" autofocus
                                value="{{ old('name') ? old('name') : $subscription->name }}" required>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea class="form-control" id="description" name="description" cols="30" rows="4"
                                style="resize: none;">{{ old('description') ? old('description') : $subscription->description }}</textarea>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="duration">Duration</label>
                                <input type="number" class="form-control" id="duration" name="duration" min="1"
                                    value="{{ old('duration') ? old('duration') : $subscription->duration }}">
                            </div>
                            <div class="form-group col-md-8">
                                <label for="unit">Unit</label>
                                <select id="unit" name="unit" class="form-control">
                                    <option value="day" {{ $subscription->unit == 'day' ? 'selected' : '' }}>Day</option>
                                    <option value="week" {{ $subscription->unit == 'week' ? 'selected' : '' }}>Week
                                    </option>
                                    <option value="month" {{ $subscription->unit == 'month' ? 'selected' : '' }}>Month
                                    </option>
                                    <option value="year" {{ $subscription->unit == 'year' ? 'selected' : '' }}>Year
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="price">Price</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Rp.</div>
                                </div>
                                <input type="number" class="form-control" id="price" name="price" min="1000"
                                    value="{{ old('price') ? old('price') : $subscription->price }}">
                            </div>
                        </div>
                        <div class="row mb-1">
                            <div class="col">
                                <div class="d-flex justify-content-end text-align-center flex-column flex-md-row">
                                    <a type="button" href="javascript:;" onclick="history.back()"
                                        class="btn btn-outline-secondary mb-2 mb-lg-0 mb-md-0 mr-0 mr-md-2 mr-lg-2">Cancel</a>
                                    <button type="submit" class="btn btn-primary"><strong>Update</strong></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col"></div>
    </div>
@endsection
