<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnsInOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('payment_status')->change();
            $table->datetime('payment_date')->nullable()->change();
            $table->datetime('end_date')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            // $table->enum('payment_status', ['1', '2', '3', '4'])->comment('1 = menunggu pembayaran, 2 = sudah dibayar, 3 = kadaluarsa, 4 = batal')->change();
            $table->datetime('payment_date')->change();
            $table->datetime('end_date')->change();
        });
    }
}
